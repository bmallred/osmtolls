MAKEFLAGS += --silent

all: test embed build
.SILENT: all
.PHONY: all

test:
	go test ./...

embed:
	cd assets; embed . .html .css .js; cd -;

build: build-linux build-macos build-windows
build-linux: build-linux-amd64
build-linux-amd64:
	GOOS=linux GOARCH=amd64 go build -o dist/linux/tolls ./cmd/tolls
build-macos: build-macos-amd64 build-macos-386
build-macos-amd64:
	GOOS=darwin GOARCH=amd64 go build -o dist/macos/tolls ./cmd/tolls
build-macos-386:
	GOOS=darwin GOARCH=386 go build -o dist/macos/tolls32 ./cmd/tolls
build-windows: build-windows-amd64 build-windows-386
build-windows-amd64:
	GOOS=windows GOARCH=amd64 go build -o dist/windows/tolls ./cmd/tolls
build-windows-386:
	GOOS=windows GOARCH=386 go build -o dist/windows/tolls32 ./cmd/tolls
