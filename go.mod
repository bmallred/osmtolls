module gitlab.com/bmallred/osmtolls

go 1.14

require (
	github.com/paulmach/go.geo v0.0.0-20180829195134-22b514266d33
	github.com/paulmach/go.geojson v1.4.0 // indirect
	github.com/qedus/osmpbf v1.1.0
	github.com/syndtr/goleveldb v1.0.0
	github.com/tmthrgd/go-popcount v0.0.0-20190904054823-afb1ace8b04f
)
