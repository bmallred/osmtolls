package main

import (
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/osmtolls/tolls/assets"
	"gitlab.com/osmtolls/tolls/open"
	"gitlab.com/osmtolls/tolls/osm"
)

const address = "localhost:8888"

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
	builtBy = "unknown"
)

func main() {
	// Declare routes
	http.HandleFunc("/", IndexHandler)
	http.HandleFunc("/static/tolls.js", StaticJavascriptHandler)
	http.HandleFunc("/static/tolls.css", StaticStylesheetHandler)
	http.Handle("/data/", http.StripPrefix("/data/", http.FileServer(http.Dir("data/"))))

	// Open the browser
	open.Start("http://" + address)

	log.Printf("tolls %s, commit %s, built at %s by %s\n", version, commit, date, builtBy)
	log.Println("Server running at " + address)
	log.Fatal(http.ListenAndServe(address, nil))
}

func StaticJavascriptHandler(w http.ResponseWriter, r *http.Request) {
	data, err := assets.Read("assets/tolls.js", true)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type", "text/javascript; charset=utf-8")
	fmt.Fprintf(w, string(data))
}

func StaticStylesheetHandler(w http.ResponseWriter, r *http.Request) {
	data, err := assets.Read("assets/tolls.css", true)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type", "text/css; charset=utf-8")
	fmt.Fprintf(w, string(data))
}

type IndexModel struct {
	OsmData string
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	osm := ""
	region := r.URL.Query().Get("region")
	if region != "" {
		pbf, _ := download(region)
		jsonl, err := extract(pbf)
		if err == nil {
			osm = jsonl
		}
	}

	data, err := assets.Read("assets/index.html", true)
	if err != nil {
		log.Fatal(err)
	}

	tmpl, err := template.New("index").Parse(string(data))
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl.Execute(w, IndexModel{
		OsmData: osm,
	})
}

func download(region string) (string, error) {
	filename := region + "-latest.osm.pbf"

	dir := "data"
	if _, err := os.Stat(dir); err != nil && os.IsNotExist(err) {
		os.Mkdir(dir, os.ModePerm)
	}

	// Check if the file already exists
	filepath := dir + "/" + filename
	if _, err := os.Stat(filepath); err == nil {
		return filepath, err
	}

	// Get the data
	url := "https://download.geofabrik.de/north-america/us/" + filename
	log.Print("Downloading data from " + url)
	response, err := http.Get(url)
	if err != nil {
		log.Print("Download failed!")
		return filepath, err
	}
	defer response.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		log.Println("Failed to create file at " + filepath)
		return filepath, err
	}
	defer out.Close()

	// Write the body of the file
	_, err = io.Copy(out, response.Body)
	return filepath, err
}

func extract(filepath string) (string, error) {
	json := filepath + ".json"

	// Check if the file already exists
	if _, err := os.Stat(json); err == nil {
		// Read file contents here
		log.Print("Reading cached data from " + json)
		data, err := ioutil.ReadFile(json)
		if err != nil {
			log.Print("Reading cached data failed!")
			return "", err
		}
		return string(data), nil
	}

	// Create the file
	out, err := os.Create(json)
	if err != nil {
		return "", err
	}
	defer out.Close()

	log.Print("Extracting tolling information from " + filepath)
	osm.ExtractJSON(filepath, out)

	log.Print("Reading data from " + json)
	data, err := ioutil.ReadFile(json)
	if err != nil {
		log.Print("Reading data failed!")
		return "", err
	}
	return string(data), nil
}
