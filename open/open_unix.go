// +build !windows,!darwin

package open

import "os/exec"

func open(address string) *exec.Cmd {
	return exec.Command("xdg-open", address)
}

func openWith(address string, appName string) *exec.Cmd {
	return exec.Command(appName, address)
}
