// +build darwin

package open

import "os/exec"

func open(address string) *exec.Cmd {
	return exec.Command("open", address)
}

func openWith(address string, appName string) *exec.Cmd {
	return exec.Command("open", "-a", appName, address)
}
