package open

// Start a browser with the given address.
func Start(address string) {
	open(address).Start()
}
