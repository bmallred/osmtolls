// +build windows

package open

import (
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

const cmd = "url.dll,FileProtocolHandler"

var runDll32 = filepath.Join(os.Getenv("SYSTEMROOT"), "System32", "rundll32.exe")

func clean(address string) string {
	r := strings.NewReplacer("&", "^&")
	return r.Replace(address)
}

func open(address string) *exec.Cmd {
	return exec.Command(runDll32, cmd, address)
}

func openWith(address string, appName string) *exec.Cmd {
	return exec.Command("cmd", "/C", "start", "", appName, clean(address))
}
