var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 8,
    });
    processOsmData(new Event('init'));
}

var markers = [];
function clearOverlays() {
    while (markers.length) {
        var marker = markers.pop();
        marker.setMap(null);
        marker = null;
    }
}

function addMarkers(lines) {
    var centerReset = false;
    var idx = 0;
    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];
        // console.info('line', line);
        if (!(line || '').trim()) {
            continue;
        }

        var tag = JSON.parse(line || {});
        if (!tag) {
            continue;
        }
        // console.info('tag', tag);

        var position = null;
        if (tag.lat && tag.lon) {
            position = {lat: tag.lat, lng: tag.lon};
        } else if (tag.tags.centroid) {
            // May need to check this since it is commonly associated w/ a "way" type
            // and also includes "bounds".
            position = {lat: tag.tags.centroid.lat, lng: tag.tags.centroid.lon};
        }

        if (!position) {
            continue;
        }

        if (!centerReset) {
            centerReset = true;
            map.setCenter(position);
        }

        var marker = new google.maps.Marker({ position: position, map: map });
        // console.log(marker);
        marker.extra = {
            id: tag.id,
            title: tag.id.toString(),
            roadway: '',
            assetType: 'onramp',
            direction: 'n',
        };
        marker.addListener('click', showToll);

        markers.push(marker);
        idx++;
    }
}

function processOsmData(e) {
    if (e.preventDefault) {
        e.preventDefault();
    }

    // Clear previous markers
    clearToll();
    clearOverlays();

    // Get the OSM data and start to create markers
    var textarea = document.querySelector('#osm > textarea');
    var lines = textarea.value.split('\n');
    addMarkers(lines);

    return false;
}

function markerBy(lat, lng) {
    for (var i = 0; i < markers.length; i++) {
        var marker = markers[i];
        if (marker.position.lat() === lat && marker.position.lng() === lng) {
            return marker;
        }
    }
    return null;
}

function showToll(e) {
    var marker = markerBy(e.latLng.lat(), e.latLng.lng())

    var extra = marker.extra;
    if (!extra) {
        return;
    }
    // console.info('extra', extra);

    var id = document.querySelector('form#toll input#id');
    id.value = extra.id;
    var title = document.querySelector('form#toll input#title');
    title.value = extra.title;
    var roadway = document.querySelector('form#toll input#roadway');
    roadway.value = extra.roadway;
    var assetType = document.querySelector('form#toll select#type');
    assetType.value = extra.assetType;
    var direction = document.querySelector('form#toll select#direction');
    direction.value = extra.direction;
}

function processToll(e) {
    if (e.preventDefault) {
        e.preventDefault();
    }

    var id = document.querySelector('form#toll input#id').value;
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].extra.id.toString() === id) {
            var title = document.querySelector('form#toll input#title');
            markers[i].extra.title = title.value;
            var roadway = document.querySelector('form#toll input#roadway');
            markers[i].extra.roadway = roadway.value;
            var assetType = document.querySelector('form#toll select#type');
            markers[i].extra.assetType = assetType.value;
            var direction = document.querySelector('form#toll select#direction');
            markers[i].extra.direction = direction.value;

            markers[i].setLabel(`${roadway.value}: ${title.value} (${direction.value})`);
        }
    }

    return false;
}

function clearToll() {
    document.querySelector('form#toll input#id').value = '';
    document.querySelector('form#toll').reset();
}

function removeToll() {
    var answer = confirm('Are you sure you want to delete this toll?');
    if (!answer) {
        return;
    }

    var id = document.querySelector('form#toll input#id').value;
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].extra.id.toString() === id) {
            var marker = markers.splice(i, 1)[0];
            marker.setMap(null);
            marker = null;
            clearToll();
        }
    }
}

// Handle manually submitting OSM data (hidden by default)
var osmForm = document.querySelector('form#osm');
if (osmForm.attachEvent) {
    osmForm.attachEvent('submit', processOsmData);
} else {
    osmForm.addEventListener('submit', processOsmData);
}

// Handle updating a toll
var tollForm = document.querySelector('form#toll');
if (tollForm.attachEvent) {
    tollForm.attachEvent('submit', processToll);
} else {
    tollForm.addEventListener('submit', processToll);
}

// Handle removing a toll
var tollRemove = document.querySelector('form#toll button[type="button"]');
tollRemove.addEventListener('click', removeToll);

// Set the selected region if possible
var searchParams = new URLSearchParams(window.location.search);
var paramRegion = searchParams.get('region');
if (paramRegion) {
    var option = document.querySelector('option[value="'+paramRegion+'"]');
    if (option) {
        option.selected = 'selected';
    }
}
